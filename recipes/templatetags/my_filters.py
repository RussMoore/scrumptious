from django import template

register = template.Library()

@register.filter(name="silly_string")
def silly_string(value): #only takes one argument
    # returns argument with ! in between the letters
    silly_string = ''
    for letter in value:
        silly_string += letter + '!'
    return silly_string

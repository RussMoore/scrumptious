from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required


# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)

    context = {
        "recipe_object": recipe,
        #"recipe_time" : Recipe.objects.get(id=id).strftime("%B")
    }
    return render(request, "recipes/detail.html", context)

@login_required
def my_recipe_list(request):
    recipes = []
    for recipe in Recipe.objects.all():
        if recipe.author == request.user:
            recipes.append(recipe)
    context = {
        "recipe_list": recipes
    }
    return render(request, "recipes/mine.html", context)


def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
        #We should use the form to validate the values
        # and save them to the database
        form = RecipeForm(request.POST)
        if form.is_valid():
            new_recipe = form.save(False)
            new_recipe.author = request.user  #this gonna work??
            new_recipe.save()
            #if all goes well, we can redirect the browser
            #  to another page and leave the function
            return redirect("home_page")
    else:
        #create an instance of the Django model form class
        form = RecipeForm()

    context = {
        "form": form,
    }
    #put the form in the context
    #Render the HTML template with the form
    return render(request, "recipes/create.html", context)

def edit_recipe(request, pk):
    recipe = Recipe.objects.get(pk=pk)
    if request.method == "POST":
        #We should use the form to validate the values
        # and save them to the database
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            #if all goes well, we can redirect the browser
            #  to another page and leave the function
            return redirect("home_page")
    else:
        #create a new instance of the Django model form class to replace
        form = RecipeForm(instance=recipe)

    context = {
        "recipe": recipe,
        "form": form,
    }
    #put the form in the context
    #Render the HTML template with the form
    return render(request, "recipes/edit.html", context)

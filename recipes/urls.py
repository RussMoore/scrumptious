from django.urls import path
from recipes.views import show_recipe, recipe_list, create_recipe, edit_recipe, my_recipe_list

urlpatterns = [
    path("", recipe_list, name='home_page'),
    path("<int:id>/", show_recipe, name="show_recipe"),
    path("create/", create_recipe, name="create_recipe"),
    path("<int:pk>/edit/", edit_recipe, name="edit_recipe"),
    path("mine/", my_recipe_list, name="mine_page"),
]
#path function takes what is in the '<' '>' and puts it into the function show_recipe

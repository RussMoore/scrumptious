from django.db import models
from django.conf import settings

class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField(max_length=250)
    description = models.TextField()

    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    star_rating = models.FloatField(null=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, # this is usually where we would call the Recipe Class
        related_name = "recipes",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.title

class RecipeStep(models.Model):
    step_number = models.PositiveIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        Recipe,
        related_name="step",
        on_delete=models.CASCADE,
    )
    class Meta:
        ordering = ["step_number"]


class RecipeIngedient(models.Model):
    amount_num = models.FloatField()
    amount_unit = models.CharField(max_length = 100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe,
        related_name="ingredients",
        on_delete=models.CASCADE,
    )

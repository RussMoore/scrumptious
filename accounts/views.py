from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from accounts.forms import SignUpForm, LogInForm

# Create your views here.
def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']

            if password == password_confirmation: #and not username in User.objects.all(): -- how would I perform this check??
                #create new user with those values
                #save to a variable
                user = User.objects.create_user(
                    username, email=None, password=password,
                    first_name=first_name, last_name=last_name
                    )
                if user is not None:
                    login(request, user)
                    return redirect('home_page')
                #login the user with the user you just created
            else:
                form.add_error("password", "Passwords do not match.. try again!")

    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)

def redirect_error(request):
    return render(request, "accounts/error_page.html")

def user_login(request):
    if request.method == "POST":
        form = LogInForm(request.POST)
        if form.is_valid():

            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home_page')
            else:
                return redirect('error_page')
    else:
        form = LogInForm()
    context = {
        "form": form,
        }
    return render(request, "accounts/login.html", context)

def user_logout(request):
    logout(request)
    return redirect('logout_page')

def logout_success(request):
    return render(request, "accounts/logout.html")

from django.urls import path
from accounts.views import redirect_error, signup, user_login, user_logout, logout_success

urlpatterns = [
    path("error/", redirect_error, name='error_page'),
    path("signup/", signup, name='signup_page'),
    path("login/", user_login, name='login_page'),
    path("logout/", user_logout, name='user_logout'),
    path("logout_successful/", logout_success, name='logout_page')

]
#path function takes what is in the '<' '>' and puts it into the function show_recipe
